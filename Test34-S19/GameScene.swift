//
//  GameScene.swift
//  Test34-S19
//
//  Created by MacStudent on 2019-06-19.
//  Copyright © 2019 rabbit. All rights reserved.
//

import SpriteKit
import GameplayKit


class GameScene: SKScene {
    
    
    var entrance:SKNode!
    var lemmings = [SKNode]()
    var platforms = [SKNode]()
    var lemings:[SKSpriteNode] = []
    var label = SKLabelNode()
    var arraydir:[String] = []
    var count:Int = 0;
    var exitcount:Int = 0;
    
    var exit:SKSpriteNode = SKSpriteNode(imageNamed: "plat2")
    
    
    
    
    override func didMove(to view: SKView) {
        
        /*for i in 1...4{
         lemmings.append(spawnlemmings())
         }
         */
        
        
        //var timeOfLastUpdate:TimeInterval = -1;
        //var spritCount = 0
        
        
    }
    
    
    func makePlatform(xPosition:CGFloat, yPosition:CGFloat) {
        
        let platform = SKSpriteNode(imageNamed: "plat")
        
        
        platform.position.x = xPosition;
        platform.position.y = yPosition;
        
        platform.zPosition = 99;
        
        
        platform.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: platform.size.width, height: platform.size.width))
        platform.physicsBody?.affectedByGravity = false
        platform.physicsBody?.categoryBitMask = 4
        platform.physicsBody?.contactTestBitMask = 1
        platform.physicsBody?.isDynamic = false
        platform.physicsBody?.pinned = false
        
        
        addChild(platform)
        
    }
    //function to create leming
    func makeleming() {
        // lets add some lemings
        let lem: SKSpriteNode = SKSpriteNode(imageNamed: "happy")
        
        lem.physicsBody = SKPhysicsBody(rectangleOf: lem.size)
        lem.physicsBody?.allowsRotation = false
        lem.physicsBody?.restitution = 0.0;
        
        lem.position = CGPoint(x: 300, y: 1080)
        
        // add the cat to the scene
        self.addChild(lem)
        
        
        // add the cat to the cats array
        self.lemings.append(lem)
        
    }
    //..............................................
    
    
    var timeOfLastUpdate:TimeInterval?
    var movingLeft = "false"
    
    override func update(_ currentTime: TimeInterval) {
        
        
        
        if (timeOfLastUpdate == nil) {
            timeOfLastUpdate = currentTime
        }
        // print a message every 3 seconds
        var timePassed = (currentTime - timeOfLastUpdate!)
        if (timePassed >= 1.5) {
            
            timeOfLastUpdate = currentTime
            // make a leming
            if(count < 15){
                self.makeleming()
                count += 1
            }
            if(count >= 15){
                if(exitcount >= 11){
                    label.text = "You Win"
                    label.fontColor = .white
                    label.color = .red
                    label.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
                    label.fontSize = 29.0
                    label.zPosition = 1
                    
                }
                else if(exitcount < 11){
                    label.text = "You Lose"
                    label.fontColor = .white
                    label.color = .red
                    label.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
                    label.fontSize = 29.0
                    label.zPosition = 1
                    
                }
                
            }
        }
        
        for (index,item) in lemings.enumerated()
        {
            if(item.intersects(exit)){
                
                lemings.remove(at: index)
                item.removeFromParent()
                arraydir.remove(at: index)
                exitcount += 1
            }
            if(item.position.y <= 40)
            {
                lemings.remove(at: index)
                item.removeFromParent()
                arraydir.remove(at: index)
            }
        }
        
        for (index,item) in lemings.enumerated()
        {
            
            if((item.position.x + item.size.width) >= self.frame.width )
            {
                movingLeft = "true"
                arraydir.insert("true", at: index)
                
            }
            if(item.position.x  <= 50)
            {
                movingLeft = "false"
                arraydir.insert("false", at: index)
            }
        }
        
        for (index,item) in lemings.enumerated()
        {
            
            let abc:SKSpriteNode = lemings[index]
            // print(self.frame.width)
            
            
            
            if(arraydir[index].elementsEqual("false")){
                abc.position.x += 10
                
            }
            else if(arraydir[index].elementsEqual("true")){
                abc.position.x -= 10}
            
        }
        
        
    }
    
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        print("you win")
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        // 1. detect where the person clicked
        let touch = touches.first!
        let mousePosition = touch.location(in:self)
        
        makePlatform(xPosition: mousePosition.x, yPosition: mousePosition.y)
        // 2b. If person click screen
    } // end touchesBegan code
    
    
    
}
